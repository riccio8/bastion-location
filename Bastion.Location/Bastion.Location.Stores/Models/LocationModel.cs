﻿namespace Bastion.Location.Stores.Models
{
    /// <summary>
    /// Location model.
    /// </summary>
    public class LocationModel
    {
        /// <summary>
        /// Get as.
        /// </summary>
        public string As { get; set; }

        /// <summary>
        /// Get city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Get country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Get country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Get isp.
        /// </summary>
        public string Isp { get; set; }

        /// <summary>
        /// Get lat.
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Get lon.
        /// </summary>
        public double Lon { get; set; }

        /// <summary>
        /// Org.
        /// </summary>
        public string Org { get; set; }

        /// <summary>
        /// Get region.
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Get region name
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// Get status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Get timezone.
        /// </summary>
        public string Timezone { get; set; }

        /// <summary>
        /// Get zip.
        /// </summary>
        public string Zip { get; set; }
    }
}