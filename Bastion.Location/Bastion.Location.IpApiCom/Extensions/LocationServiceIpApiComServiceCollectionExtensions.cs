﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using Bastion.Location.Abstractions;
using Bastion.Location.IpApiCom;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// bastion location service for ip-api.com service collection extensions.
    /// </summary>
    public static class LocationServiceIpApiComServiceCollectionExtensions
    {
        /// <summary>
        /// Add bastion location service for ip-api.com.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddLocationServiceIpApiCom(this IServiceCollection services)
        {
            services.AddHttpClient<ILocationService, LocationIpApiComService>(client =>
            {
                client.BaseAddress = LocationIpApiComService.BaseAddress;
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigurePrimaryHttpMessageHandler(handler => new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            });

            return services;
        }
    }
}