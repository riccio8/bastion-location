﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

using Bastion.Location.Abstractions;
using Bastion.Location.Stores.Models;

using Microsoft.Extensions.Logging;

namespace Bastion.Location.IpApiCom
{
    /// <summary>
    /// Location service for ip-api.com.
    /// </summary>
    public class LocationIpApiComService : ILocationService
    {
        private readonly ILogger logger;
        private readonly HttpClient httpClient;

        public static Uri BaseAddress { get; set; } = new Uri("http://ip-api.com/");

        /// <summary>
        /// Initializes a new instance of <see cref="LocationIpApiComService"/>.
        /// </summary>
        public LocationIpApiComService(
            ILogger<LocationIpApiComService> logger,
            HttpClient httpClient)
        {
            this.logger = logger;
            this.httpClient = httpClient;
        }

        /// <summary>
        /// Get Location.
        /// </summary>
        /// <param name="ip">Ip address</param>
        public async Task<LocationModel> GetLocationAsync(string ip)
        {
            var response = await httpClient.GetAsync($"/json/{ip}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<LocationModel>();
        }
    }
}