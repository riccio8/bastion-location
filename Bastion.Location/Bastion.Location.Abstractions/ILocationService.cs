﻿using System.Threading.Tasks;

using Bastion.Location.Stores.Models;

namespace Bastion.Location.Abstractions
{
    /// <summary>
    /// Interface location service.
    /// </summary>
    public interface ILocationService
    {
        /// <summary>
        /// Get Location.
        /// </summary>
        /// <param name="ip">Ip address</param>
        Task<LocationModel> GetLocationAsync(string ip);
    }
}